---
title: Download
---
# Downloads

Here you can get all you need to start making your own Alchemy application, or contribute to the Alchemy project.

## Alchemy

For your convenience, we provide several pre-built packages that include Alchemy headers and libraries.

* [Mac OS X, 10.6+, 32-bit, libc++][Alchemy-Mac-32bit]
* [Windows, 32-bit][Alchemy-Windows-32bit]
* [Linux, 32-bit][Alchemy-Linux-32bit]
* [Linux, 64-bit][Alchemy-Linux-64bit]

If your OS, architecture, or standard library combination is not listed above, you'll need to [build Alchemy from source][Build-Instructions].

## CEF

Because Alchemy is built on the Chromium Embedded Framework (CEF), whether you're building your own application or working on Alchemy itself, you'll need to include some CEF resources and libraries. While we provide some pre-built packages that you can drop right in, you can always [build your own CEF binaries][CEF].

* [Mac OS X, 10.6+, 32-bit][CEF-Mac-32bit]
* [Windows, 32-bit][CEF-Windows-32bit]
* [Linux, 32-bit][CEF-Linux-32bit]
* [Linux, 64-bit][CEF-Linux-64bit]

For instructions on how to integrate the CEF resources and libraries into your project, check out the [Build Instructions page][Build-Instructions]

## GYP

In order to build Alchemy from source, you'll need Google's GYP (Generate Your Project) tool. See our [GYP Reference page](<%= url('/reference/gyp/') %>) for details on how to install and use GYP.

[CEF]: https://code.google.com/p/chromiumembedded/

[Build-Instructions]: <%= url('/reference/build/') %>

[GYP-Download]: https://bitbucket.org/libalchemy/libalchemy/downloads/gyp.tar.gz

[Alchemy-Mac-32bit]: #
[Alchemy-Windows-32bit]: #
[Alchemy-Linux-32bit]: #
[Alchemy-Linux-64bit]: #

[CEF-Mac-32bit]: https://bitbucket.org/libalchemy/libalchemy/downloads/mac_32bit_cef1_1364_1123.tar.gz
[CEF-Windows-32bit]: #
[CEF-Linux-32bit]: #
[CEF-Linux-64bit]: #