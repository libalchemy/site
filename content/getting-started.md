---
title: Getting Started
show_toc: true
---

# Introduction

Alchemy was designed to solve a specific user-interface problem: on one hand, web applications can be quite beautiful and easy to design, but are pretty much limited to server-client applications served within the browser's sandbox. On the other hand, desktop applications -- especially those written in C++ -- have very complex and heavy-weight graphical user-interface libraries (Swing/AWT for Java; Qt/GTK/wxWidgets for C/C++; WinForms/WPF for .NET), but are very powerful since they have full, unfettered access to the user's computer. Sure, there are form designers provided by IDEs, but for anything beyond simple UI layouts, you're quickly forced into writing that code by hand.

Wouldn't it be nice to take the best of both worlds?

Alchemy allows developers to write their user-interface using HTML, CSS, JavaScript, and any other client-side technologies supported by modern web browsers, then show it and interact with it via a simple cross-platform C++ API.

# Hello World

Creating a UI with Alchemy is very simple. Let's set up a basic "Hello World" project.

Suppose we have the following files:

~~~
alchemy-hello-world/
    ui/
        hello.html
    hello.cpp
~~~


Here's what `hello.html` looks like:

~~~ html
<html>
<head><title>Hello World</title></head>
<body><h1>Hello, World!</h1></body>
</html>
~~~

And here's `hello.cpp`:

~~~ cpp
#include "alchemy/alchemy.h"

int main() {
    Alchemy::Settings settings;
    settings.DefaultWindowSize(800, 600);
    settings.Directory("ui");

    auto app = Alchemy::Application::Create(settings);
    app->Show("hello.html");
}
~~~

As you can see, all we need do is set some settings using the `Alchemy::Settings` object, then create a new `Alchemy::Application` object from it, and show the HTML page.

There are two important method calls here. `Settings.Directory(directory)` tells Alchemy where to look for any HTML pages, **relative to the final executable**. `Application.Show(page)` will open a new window, load the given file (relative to the directory given by the `Settings.Directory()` call), and block until the window is closed.

If you're wondering why we declare `app` as a type-deduced variable, it's because `Application::Create()` returns an `std::unique_ptr<Alchemy::Application>`, and that's a lot of typing for something that just behaves like a pointer.

# JavaScript Integration

Now that we've covered the basics, let's make our Hello World application a little more dynamic, by passing data back and forth from the C++ back-end to the JavaScript front-end.

## Function Binding

Suppose we wanted to say hello not to the world, but to the user running the program. How can we do that? Let's make it so that when the page loads, our UI asks the back-end for a name, which in turn asks the user for his or her name, then updates the UI with that name.

We'll tweak the above files as follows.

First up, let's change the HTML file to look like this:

~~~ html
<html>
<head>
    <title>Hello World</title>
    <script type="text/javascript">
        window.onload = function() {
            document.getElementById("name").innerText = app.getName();
        };
    </script>
</head>
<body>
    <h1>Hello, <span id="name"></span></h1>
</body>
</html>
~~~

There's nothing too special about the above code, except for the magic `app` variable. This is a JavaScript object populated at run-time by Alchemy with natively implemented functions. But how to do that?

In your `hello.cpp` file, we need to inform Alchemy that we have just such a function, and of course implement that function:

~~~ cpp
#include "alchemy/alchemy.h"
#include <iostream>
#include <string>

using namespace std;

string getName() {
    string name;

    cout << "What's your name? ";
    getline(cin, name);

    return name;
}

int main() {
    Alchemy::Settings settings;
    settings.DefaultWindowSize(800, 600);
    settings.Directory("ui");

    auto app = Alchemy::Application::Create(settings);
    app->Page("hello.html")->Bind("getName", getName);
    app->Show("hello.html");
}
~~~

All the `getName()` function does is prompt the user for a name on a console and return the string they enter. The magic, however, happens with the `app->Page("hello.html")->Bind("getName", getName);` line.

Three things are happening here. First, we retrieve the `Alchemy::Page` instance corresponding to the `hello.html` file. An `Alchemy::Page` is just a container holding various attributes about the pages that Alchemy loads.

Second, we *bind* the `getName` function to the JavaScript name "getName". This means that when you call `window.app.getName()` from the front-end, you are actually invoking the `getName` function defined in `hello.cpp`.

Third, and this one you might have missed entirely, is that Alchemy automatically detects the argument and return types of the function you pass to the `Bind` method, and uses that to do run-time conversion to and from JavaScript types and C++ types. You can read more about this in the [Reference](<%= url('/reference/javascript/') %>).

If you run this, you will indeed see that it does exactly what we want: when the window loads, prompt the user for a name and display it in the header. Nifty.

## Function Calling

So how can we call front-end functions from the back-end? Alchemy provides the Page `Call` function, which will invoke a JavaScript function attached to a page's `app` object.

Let's make a simple function on the front-end which adds items to a list. We will then call this function with information obtained from the back-end.

The front-end could look something like this:

~~~ html
<html>
<head>
    <title>To-Do List</title>
</head>
<body>
    <script type="text/javascript">
        app.addToDo = function (item) {
            ol = document.getElementById('todo');
            li = document.createElement('li');
            li.innerText = item;
            ol.appendChild(li);
        }
    </script>

    To-Do List:<br>
    <ol id="todo"></ol>
</body>
</html>
~~~

This front-end code ties the `addToDo` function to our `app` object, which makes it available to the back-end.

In order to call the `addToDo` function from the back-end we need to use a worker thread. With C++11's `std::thread`, this task becomes relatively simple. Here's the back-end code:

~~~ cpp
#include "alchemy/alchemy.h"
#include "alchemy/js/js.h"
#include <chrono>
#include <thread>

using namespace std;

unique_ptr<Alchemy::Application> app;

void callFunction() {
    auto index = app->Page("index.html");

    while(!index->Ready()) {
        this_thread::sleep_for(chrono::milliseconds(100));
    }

    //function calling
    index->Call("addToDo", "Task 1");
    index->Call("addToDo", "Task 2");
}

int main() {
    Alchemy::Settings settings;
    settings.DefaultWindowSize(800, 600);
    settings.Directory("ui");

    auto app = Alchemy::Application::Create(settings);

    thread t(callFunction);

    app->Show("index.html");
}
~~~

As you can see, we create our Application and then spin up a worker thread to call into the front-end.

It is important to note that we must first wait until the page is ready before calling into the front-end. This ensures that the contents of the page have finished loading before proceeding.

Once the page is ready, we can call the front-end function `addToDo`, inserting our items into the to-do list.

Expanding upon this example, it would be easy to use Alchemy's function calling to update the front-end with information from network, file, or database transactions.
