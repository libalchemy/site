---
title: Home
---
# Bring the web to your desktop

Alchemy aims to allow developers to rapidly design user interfaces for their desktop applications using familiar front-end technologies like HTML5, CSS3, JavaScript, SVG, and more.

## Built on Greatness

Alchemy is built on the [Chromium](http://www.chromium.org/) web browser and the [Chromium Embedded Frame](https://code.google.com/p/chromiumembedded/) project to provide a reliable base for building with the latest and greatest web standards.

## Simple

Alchemy provides a simple C++ library handcrafted for being easy to use and integrate into existing applications by maintaining a distinct separation of concerns between your front-end UI and back-end application.

## Open Source

Alchemy, this website, and several example projects are freely available under the [MIT Software License](http://opensource.org/licenses/MIT), and can be forked and contributed to on [BitBucket](http://bitbucket.org/libalchemy).

## Cross Platform

Alchemy works out of the box on the Windows, Mac OS X, and Linux operating systems.

<p style="text-align:center">
<img src="<%= url('/img/platforms/') %>">
</p>