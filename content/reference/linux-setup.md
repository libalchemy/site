---
title: Developer Setup (Linux)
show_toc: true
---

# Introduction

This guide is intended to provide Linux developers step-by-step instructions on setting up Alchemy. Included are instructions on using GYP (Generate Your Project) and manual build instructions for Alchemy's dependencies.

**Heads Up!** You only need to use these instructions if you need to build Alchemy from source
{: .alert .alert-warning}

# Build-Essentials Setup {#build-essentials}

## Build-Essentials

This tutorial requires the use of the build-essentails package. Alchemy relies upon C++11 features that are availabe in the compiler that this package provides.

## Build-Essentials Installation

Installation is easy. Simply open a new terminal then execute the command `sudo apt-get install build-essentials`
Once this command finishes, the build-essentials package will be done installing and ready for use.

# GYP (Generate Your Project)  {#gyp} 

Alchemy uses GYP to ease the burden of cross-platform project generation. Linux developers who desire to recreate the Alchemy projects can do so using GYP.

Follow the instructions outlined in the [GYP Reference Page](<%= url('/reference/gyp/') %>) to install and use GYP.

Go ahead and generate the project using GYP:

    GYP_PATH/gyp --depth=. alchemy.gyp

# Dependencies  {#dependencies} 

Below are instructions to manually build Alchemy's dependencies. Visit the Alchemy's download page for prebuilt dependencies.

`libcef`, `libcef_dll_wrapper`, and the Boost headers are required by Alchemy, while Google Test and the Boost `filesystem` and `system` libraries are only needed for Alchemy's tests and the File Finder example project, respectively.

## Chromium Embedded Framework

### libcef

A prebuilt binary of `libcef` is provided by Marshall A. Greenblatt, owner of the Chromium Embedded Framework project. You can [get it here](http://www.magpcss.net/cef_downloads/). The current version used by Alchemy is named `cef_binary_3.1364.1188_linuxXX.7z`, but newer versions should work.

Download and extract the archive to a temporary directory.

Navigate to the `libalchemy` directory within your workspace. Create a directory here named `cef` if one does not yet exist. This is where we will place all of the binaries and headers needed for the Chromium Embedded Framework.

From the `cef_binary_3.1364.1188_linux` directory, copy the `include` directory into `libalchemy/cef/`. (So you should now have, `libalchemy/cef/include`.)

Create a new directory, `runtime`, in `libalchemy/cef`. Copy everything except `cefclient.exe` from `cef_binary_3.1364.1188_linux/Release` into the `libalchemy/cef/runtime` directory.

Copy `libcef.lib` from `cef_binary_3.1364.1188_linux/lib/Release` into `libalchemy/cef`.

### libcef\_dll\_wrapper

We provide a pre built linux dll wrapper for your convenience. Get it [here](<%= url('/downloads/') %>).

## Google Test

If you wish to run or contribute to our unit test project, you will need to use Google Test. The Google Test source can be [downloaded here](https://code.google.com/p/googletest/downloads/list) from Google Code.

Extract the archive to a directory by executing the command `unzip /path/to/gtest-1.6.0`

Create a build directory `mkdir /path/to/mybuild` to make gtest and navigate to it.

Now we are going to make gtest using cMake. This is done in a few simple commands.

    cmake /path/to/gtest-1.6.0
    make

After running these two commands your `mybuild` directory should have `libgtest.a` and `libgtest_main.a` inside of it as well as some other files.

Copy `libgtest.a` and `libgtest_main.a` into your `libalchemy/gtest` directory. (So your `libalchemy/gtest` directory now contains the necessary headers for Google Tests)

Google Test is now configured for Alchemy.

You can follow Google Test's [documentation](https://code.google.com/p/googletest/wiki/Documentation) to build and deploy Google Test unit tests on your own.

## Boost

The Boost headers are required by Alchemy, however if you would like to run the File Finder example project, you will also need the Boost `system` and `filesystem` libraries.

Visit the [Boost Downloads page](http://www.boost.org/users/download/) and under `Current Release`, click the `Download` link. This will take you to SourceForge.

Download the Boost archive. [boost\_1\_53\_0.tar.bz2](http://sourceforge.net/projects/boost/files/boost/1.53.0/boost_1_53_0.tar.bz2/download) was used for Alchemy.

Navigate to the directory where you want to install boost. Now extract Boost:

    tar --bzip2 -xf /path/to/boost_1_53_0.tar.bz2

### Installing Boost Headers

***TODO:** Install ONLY Boost headers*

### Building Boost Libraries: filesystem and system

The File Finder example project requires two Boost libraries: filesystem and system. Here are instructions to build them.

Open a terminal and navigate to the `boost_1_53_0` folder.

Enter the command

    ./bootstrap.sh --prefix=/usr/local --with-libraries=filesystem,system

Once this completes execute the following command to build our libraries:

    ./b2 -a link=static install

Once the libraries are built, they will be placed into `/usr/local` and are ready to go.