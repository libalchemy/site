---
title: Application Class
---

# Application Class

The Application class is essentially the center of Alchemy.

An Application object is created by giving it a Settings object that has been configured to the developer's liking.

Once an Application object has been created, it becomes your window into Alchemy.

From this object you can do the following:

* Show a given page.
* Close the active window.
* Return a page object.
* Tell you whether a page exists.
* Get the path to the current execution directory.
* Give you the project's Window object.
* Give you the project's Settings object.

A simple example of using the Application object is as follows:

~~~ cpp
#include "alchemy/alchemy.h"

int main() {
    Alchemy::Settings settings;
    settings.DefaultWindowSize(800, 600);
    settings.Directory("ui");

    auto app = Alchemy::Application::Create(settings);
    app->Show("hello.html");
}
~~~

For most basic applications, the only real interaction with the Application object you will need is creating the Application object with a Settings object and telling the Application object to show a page.

It really is that simple!

Check out the API documentation for a more detailed view of all of the functions of the Application class.