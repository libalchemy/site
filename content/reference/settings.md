---
title: Settings Class
---

# Settings Class

The Settings class is the first building block of Alchemy.

It is created as follows:

~~~ cpp
#include "alchemy/alchemy.h"

int main() {
    Alchemy::Settings settings;
}
~~~

With the Settings class you currently have access to 3 different settings.

* Changing the directory the UI is located at.
* Changing the default Window size.
* Whether to show Developer tools or not.

These settings can be changed like this:

~~~ cpp
#include "alchemy/alchemy.h"

int main() {
    Alchemy::Settings settings;
    settings.DefaultWindowSize(800, 600);
    settings.Directory("ui");
    settings.ShowDevTools(true);
}
~~~

Keep in mind that the `Directory` setting is relative to the final executable path on Windows and Linux, and on Mac, relative to the application bundle's `Resources/` directory.

Once you have these settings set you can pass the Settings object to Application and you are ready to go.

Check out the API documentation for a more detailed view of all of the functions of the Settings class.