---
title: Example Projects
---

# Example Projects

For testing purposes and to provide you with a starting point, we've come up with a few example projects.

## Owl Facts

[Owl Facts](<%= url('/reference/owlfacts/') %>) is a very simple application utilizing inline frames and front-to-back function calling that we used to exercise Alchemy while we were building it.

## File Finder

[File Finder](<%= url('/reference/filefinder/') %>) is an example of a potential real-world use of Alchemy. In it, we implement a simple and elegent file search tool utilizing external libraries, back-to-front function calling, and asynchronus control flow.