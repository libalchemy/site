---
title: Page Class
show_toc: true
---

# Page Class

The `Page` class represents the various views of your application.

An Alchemy project can contain many pages (as any web application would!).

# Creation

A page is created using the `Alchemy::Application::Page()` factory method:

~~~ cpp
#include "alchemy/alchemy.h"

int main(){
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(1100, 800));

  app = Alchemy::Application::Create(settings);

  auto index = app->Page("index.html");
}
~~~

This method will create the `Page` object the first time you call it, or it will return the same instance on repeated calls for the same named file.

The parameter passed to the `Page` method is the file name of the document you want Alchemy to load, relative to the `Directory` setting.

In the example above, `app->Page("index.html")` refers to `.../path/to/executable/ui/index.html`

Also note that calling `app->Show("index.html")` is the same as

~~~ cpp
auto index = app->Page("index.html");
app->Show(index);
~~~

# Other methods

The Page object can do the following:

* Grab the URL of the file associated with this page object.
* Tell whether the page has finished loading and its context is available.
* Bind a back end function to the Javascript window.app object of the page.
* Call a function on the Javascript window.app object.

Check out the API documentation for a more detailed view of all of the functions of the Page class.

# JavaScript Integration

Each `Page` object is responsible for managing the JavaScript bindings for the corresponding HTML document.

For a more detailed explanation of using JavaScript with Alchemy, check out the [JS Integration Page](#)