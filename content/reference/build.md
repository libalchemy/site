---
title: Build Instructions
---
# Build Instructions

**Heads Up!** You only need to use these instructions if you need to build Alchemy from source
{: .alert .alert-warning}

Visit the page corresponding to your platform:

* [Windows](<%= url('/reference/windows-setup/') %>)
* [Mac](<%= url('/reference/mac-setup/') %>)
* [Linux](<%= url('/reference/linux-setup/') %>)