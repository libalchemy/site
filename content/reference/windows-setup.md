---
title: Developer Setup (Windows/Visual Studio)
show_toc: true
---

# Introduction

This guide is intended to provide Windows developers step-by-step instructions on setting up Alchemy with Visual Studio. Included are instructions on preparing Visual Studio for Alchemy, help using GYP (Generate Your Project), and manual build instructions for Alchemy's dependencies.

**Heads Up!** You only need to use these instructions if you need to build Alchemy from source
{: .alert .alert-warning}

# Visual Studio Setup

## Visual Studio 2012

This tutorial requires the use of Visual Studio 2012. Alchemy relies upon C++11 features only contained within the November 2012 CTP compiler. This compiler only supports Visual Studio 2012.

Note: This tutorial has not been tested with Visual Studio Express 2012.

## Visual C++ Compiler November 2012 CTP

Alchemy makes extensive use of many features from C++11, a handful of which are only supported by MSVC through the November 2012 CTP preview compiler for Visual Studio 2012. Each of Alchemy's dependencies must therefore be built with this compiler in order to maintain compatibility.

To get started using this compiler, visit the [Visual C++ Compiler November 2012 CTP](http://www.microsoft.com/en-us/download/details.aspx?id=35515) page for more information or directly download [vc_compilerCTPNov2012.exe](http://download.microsoft.com/download/5/1/6/5169AA1E-D7A4-4DC2-A9AE-0A9DFD2601AB/vc_compilerCTPNov2012.exe).

Installation is easy. Simply close Visual Studio and follow the installer. Once the preview compiler is installed, Visual Studio will be ready for use with Alchemy.

# GYP (Generate Your Project) 

Alchemy uses GYP to ease the burden of cross-platform project generation. Windows developers who desire to recreate Alchemy's Visual Studio projects can do so using GYP.

Follow the instructions outlined in the [GYP Reference Page](<%= url('/reference/gyp/') %>) to install and use GYP.

Go ahead and generate the Visual Studio 2012 project using GYP:

    GYP_PATH/gyp --depth=. -G msvs_version=2012 alchemy.gyp

# Dependencies

Below are instructions to manually build Alchemy's dependencies. Visit the Alchemy's download page for prebuilt dependencies.

`libcef`, `libcef_dll_wrapper`, and the Boost headers are required by Alchemy, while Google Test and the Boost `filesystem` and `system` libraries are only needed for Alchemy's tests and the File Finder example project, respectively.

## Chromium Embedded Framework

### libcef

A prebuilt binary of `libcef` is provided by Marshall A. Greenblatt, owner of the Chromium Embedded Framework project. You can [get it here](http://www.magpcss.net/cef_downloads/). The current version used by Alchemy is named `cef_binary_1.1364.1123_windows.zip`, but newer versions should work.

Download and extract the archive to a temporary directory.

Navigate to the `libalchemy` directory within your workspace. Create a directory here named `cef` if one does not yet exist. This is where we will place all of the binaries and headers needed for the Chromium Embedded Framework.

From the `cef_binary_1.1364.1123_windows` directory, copy the `include` directory into `libalchemy\cef\`. (So you should now have, `libalchemy\cef\include`.)

Create a new directory, `runtime`, in `libalchemy\cef`. Copy everything except `cefclient.exe` from `cef_binary_1.1364.1123_windows\Release` into the `libalchemy\cef\runtime` directory.

Copy `libcef.lib` from `cef_binary_1.1364.1123_windows\lib\Release` into `libalchemy\cef`.

### libcef\_dll\_wrapper

The last CEF dependency is `libcef_dll_wrapper.lib`. This must be built with the preview compiler.

Open `cef_binary_1.1364.1123_windows\cefclient2010.sln` with Visual Studio 2012 and update the projects as asked.

Set the build configuration to `Release`.

In `Solution Explorer`, select the `libcef_dll_wraper` project, right-click and go to Properties.

Under `Configuration Properties`, select `General`.

Change the `Platform Toolset` to `Microsoft Visual C++ Compiler Nov 2012 CTP (v120_CTP_Nov2012)`, the preview compiler that we previously installed.

While we are here, take note of the `Output Directory`. Mine is `..\..\..\build\$(Configuration)\`. 

Navigate to `Configuration Properties`, then `C/C++`, then `Code Generation`. Change `Runtime Library` to `Multi-Threaded DLL (/MD)`.

You may now close the properties.

In `Solution Explorer` right-click the `libcef_dll_wrapper` project. Select `Project Only` and `Rebuild Only libcef_dll_wrapper`.

Navigate to the output directory, relative to solution (.sln) file. In my case, it is three directories up in the `build` directory. We have built the project for `Release`, so navigate to `build\Release\lib` and copy `libcef_dll_wrapper.lib` to our `libalchemy\cef` directory.

## Google Test

If you wish to run or contribute to our unit test project, you will need to build Google Test. The Google Test source can be [downloaded here](https://code.google.com/p/googletest/downloads/list) from Google Code.

Extract the archive to a temporary directory, and navigate to `gtest-1.6.0\msvc`. 

Open `gtest.sln` with Visual Studio 2012. You will need to upgrade the projects, and you may ignore the warnings.

In `Solution Explorer`, right-click on the `gtest_prod_test` and `gtest_unittest` projects and select `Unload Project`. These will not be needed.

Set the build configuration to `Release`.

In `Solution Explorer`, highlight both the `gtest` and `gtest_main` projects, and then right click and select `Properties`. The properties we change will be applied to both projects.

Navigate to `Configuration Properties`, then `C/C++`, then `Preprocessor` and insert `_VARIADIC_MAX=10;` at the beginning of the `Preprocessor Definitions` value.

Navigate to `Configuration Properties`, then `C/C++`, then `Code Generation`. Change `Runtime Library` to `Multi-Threaded DLL (/MD)`.

You may now close the properties.

Build both `gtest` and `gtest_main`, and then navigate to `gtest-1.6.0\msvc\gtest\Release`.

Copy both `gtest.lib` and `gtest_main.lib` from `gtest-1.6.0\msvc\gtest\Release` to `libalchemy\gtest`.
You may need to create a directory in `libalchemy` named `gtest` if one does not yet exist.

Copy the `gtest` directory from `gtest-1.6.0\msvc\` to `libalchemy\gtest`. (So you should now have `libalchemy\gtest\gtest`, containing Google Test's headers.)

Google Test is now configured for Alchemy.

## Boost

The Boost headers are required by Alchemy, however if you would like to run the File Finder example project, you will also need to build the Boost `system` and `filesystem` libraries.

Visit the [Boost Downloads page](http://www.boost.org/users/download/) and under `Current Release`, click the `Download` link. This will take you to SourceForge.

Download the Boost archive. [boost\_1\_53\_0.zip](http://sourceforge.net/projects/boost/files/boost/1.53.0/boost_1_53_0.zip/download) was used for Alchemy.

Navigate to `C:\Program Files` and create a directory here named `boost` if one does not yet exist.

Extract the Boost archive into the `boost` directory. (So you should now have `C:\Program Files\boost\boost_1_53_0`, or whichever version you downloaded.)

### Adding Boost to the Visual Studio 2012 default paths

Open Visual Studio 2012 with any Visual C++ project.

Navigate to `View`, then `Other Windows`, then `Property Manager`.

You will see a tree view of the projects contained within the current solution.

Pick any Visual C++ project in the Property Manager and expand it. You will now see the project's build configurations.

Expand either the `Debug` or `Release` node, and select `Microsoft.Cpp.Win32.user`. Right-click and select `Properties`. We are now editing the Visual Studio properties inherited by each project.

Navigate to `Common Properties`, then `VC++ Directories`. 

Select `Include Directories` and insert `C:\Program Files\boost\boost_1_53_0;` at the beginning of the value.

Select `Library Directories` and insert `C:\Program Files\boost\boost_1_53_0\stage\lib;` at the beginning of the value.

You may now close the properties.

Finally, with `Microsoft.Cpp.Win32.user` still selected, click the save icon in the Property Manager. You have just updated the default Visual Studio properties.

### Building Boost Libraries: filesystem and system

The File Finder example project requires two Boost libraries: filesystem and system. Here are instructions to build them.

Open command prompt and navigate to `C:\Program Files\boost\boost_1_53_0`.

Execute the following command to prepare the Boost.Build system:  
`bootstrap --with-libraries=filesystem,system`

Once this completes, execute the following command to build our libraries:  
`b2 toolset=msvc-11.0 link=static threading=multi`  
This step may take a few minutes.

Once the libraries are built, they will be placed into 
`C:\Program Files\boost\boost_1_53_0\stage\lib` and are ready to go.

# Notes

## GYP

If multiple projects within a solution each copy files on build, the order projects are built will effect the outcome (consider projects copying different files with the same name). In order to specifically target a single project to build (and its associated files to copy), right-click the desired project in `Solution Explorer`, select `Project Only`, and `Rebuild Only project`. This can cause confusion when multiple projects in a solution each contain their own UI directory.