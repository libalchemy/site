---
title: JavaScript Integration
show_toc: true
---
# JavaScript Integration

What good is a User Interface library if it's not dynamic?

Alchemy was built with tight JavaScript integration as one of its goals. As a result, you can seamlessly pass JavaScript and (some) native C++ objects back and forth, as well as call front-end JS functions and back-end C++ functions.

# Function Binding

What Alchemy refers to "Function Binding" is the process of binding a back-end, natively implemented C++ function to a JavaScript function callable from the front-end.

Because this is highly dependent on what page is using that back-end function, you can only bind a function through the use of the `Alchemy::Page::Bind()` method:

~~~ cpp
page->Bind("jsName", cppName);
~~~

The `Bind` method takes a string representing the JavaScript name of the function, and a function pointer. Through some compile-time meta-programming magic, Alchemy can deduce the argument and return types of the function, and use that to perform run-time type checking of every call to a bound function.

On the front-end, these bound functions are available through the `window.app` object, and are by default invoked with the global context. Any arguments passed to the function are transferred directly to the back-end function, and any return value that can be mapped to a JavaScript type will be returned to the front-end. Keep in mind that only the pages that they are bound to have access to them.

From the above snippet:

~~~ javascript
var result = app.jsName(...); // default global scope
var result = app.jsName.call(obj, ...); // explicit scope
~~~

## Asynchronicity

One large problem with JavaScript running in the browser is that it is single threaded. This unfortunately also applies to Alchemy. If your back-end function takes a while to return, your user interface will be frozen for the duration of the function call.

To resolve this, you can use [continuation-passing style](http://en.wikipedia.org/wiki/Continuation-passing_style) and a [thread](http://en.cppreference.com/w/cpp/thread/thread) to provide maximum usability:

~~~ cpp
void getName(const Alchemy::JS::Function& whenDone) {
    thread t([&]{
        string name;

        cout << "What's your name? ";
        getline(cin, name);

        whenDone(name);
    });
}

...

page->Bind("getName", getName);
~~~

~~~ javascript
app.getName(function(name){
    alert(name);
});
~~~

This method takes advantage of Alchemy's ability to wrap and manipulate JavaScript functions, through the `Alchemy::JS::Function` class. See the [Type Conversion](#type-conversion) section below for details.

**Note:** Because this is such a common scenario, it's on our roadmap to make a more convenient asynchronous JS API.
{: .alert .alert-info}

# Function Calling

In order to support passing information directly from the back-end to the front-end without first requesting it, Alchemy allows you to directly call any JavaScript function on the `window.app` object, whether it was attached by the back-end `Page::Bind` method or by JavaScript:

~~~ cpp
int result = page->Call("add", 4, 5); // = 9
~~~

~~~ javascript
window.app.add = function(arg1, arg2) { return arg1 + arg2 };
~~~

This feature is very tricky to utilize properly, however. Because the front-end function is only available while that specific page is loaded, you must be very careful about when you try to invoke it. We recommend either setting a variable using a bound function or periodically checking to ensure the page is loaded using the `Page::IsLoaded()` method.

# Type Conversion

Alchemy automatically handles type conversion from C++ to JavaScript and vice versa at runtime.

Conversions between the following types are supported:

|---
| **C++** | **JavaScript**
|---
| int | number
| unsigned int | number
| double | number
| bool | boolean
| std::string | string
| Alchemy::JS::Function | function
| Alchemy::JS::Object | object
| Alchemy::JS::Array | array
|---

When calling functions from the back-end, the provided arguments are automatically translated into their respective JavaScript types. Conversely, front-end calls into bound C++ functions are translated into C++ types.

Alchemy will perform run-time type checking to ensure that the source type is compatible with the destination type, and when the types differ a run-time exception is thrown.

When calling bound C++ functions from the front-end, the JavaScript arguments must match the C++ function signature. If the amount of arguments or their respective types do not agree, Alchemy will throw a run-time exception. However, calling a front-end JavaScript function from the back-end is not restricted in this way. JavaScript functions can accept varying amounts of arguments (accessible through the arguments array), regardless of the function signature. As such, Alchemy will simply translate any and all C++ arguments into JavaScript types, and pass them through to the JavaScript function.

When a JavaScript function is called from the back-end, the return type - if any - is determined based upon the type of the variable receiving the return value. This is best demonstrated through an example:

~~~ cpp
//return value is converted to int
int sum = index->Call("add", 1.5, 2);

//return value is converted to double
double square = index->Call("square", 16);
~~~

Because of the way this works, type deduction with the C++11 `auto` keyword cannot be used for return values, as Alchemy uses the provided C++ type when converting from JavaScript to C++.

## NativeType

Under the covers, Alchemy uses an `Alchemy::JS::NativeType` object to wrap a JavaScript value. Using implicit conversion constructors and conversion operators, Alchemy allows a very fluent way of accessing the underlying type. Because of this, however, you'll find that sometimes the compiler can get confused and complain about ambiguous arguments or operators. To get around this, anywhere there is that implicit conversion (function arguments and returns, mostly) you can instead use an `Alchemy::JS::NativeType`, and work with the wrapped value instead.

As an added bonus, this also means that if you really want to, you can create completely dynamically typed native functions:

~~~ cpp
using Alchemy::JS::NativeType;

NativeType doSomething(NativeType arg0, NativeType arg1, NativeType arg2 = false) {
    if (arg0.IsArray()) {
        // return a number
        return arg0.GetArray().Length();
    }
    if (arg0.IsString()) {
        // return a string
        return arg0.GetString();
    }
    if (arg0.IsBool() && arg0 == false) {
        // return any NativeType
        return arg1;
    }
}
~~~

You can read more about the `NativeType` class in the API documentation.