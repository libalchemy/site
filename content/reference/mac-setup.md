---
title: Developer Setup (Mac OSX/Xcode)
show_toc: true
---

# Introduction

This guide is intended to provide Mac developers step-by-step instructions on setting up Alchemy with Xcode. Included are instructions on preparing Xcode for Alchemy, help using GYP (Generate Your Project), and manual build instructions for Alchemy's dependencies.

**Heads Up!** You only need to use these instructions if you need to build Alchemy from source
{: .alert .alert-warning}

# Xcode Setup

## Xcode

This tutorial requires the use of Xcode. Alchemy relies upon C++11 features that are available in the compiler used by Xcode.

## Xcode Command Line Tools

Alchemy makes extensive use of features from C++11, these features are supported through the compiler found with Xcode Command Line Tools. Though Xcode itself (found in the Mac App Store) is not required, the Xcode Command Line Tools are. If you choose to install Xcode the command line tools will come with Xcode. If you would only like to install the Command Line Tools, visit the [Xcode Command Line Tools](https://developer.apple.com/downloads/index.action#) page to download them. Note that in order to download the tools, you will have to create or log in to your Apple developer account. Each of Alchemy's dependencies must be built with the compiler bundled with these tools in order to maintain compatibility.

Installation is easy. Once the DMG file has been downloaded, open it, open the new MPKG that appears, and follow the install instructions. Once installed the tools are ready for use with Alchemy.

# GYP (Generate Your Project)

Alchemy uses GYP to ease the burden of cross-platform project generation. Mac developers who desire to recreate Alchemy's XCode projects can do so using GYP.

Follow the instructions outlined in the [GYP Reference Page](<%= url('/reference/gyp/') %>) to install and use GYP.

Go ahead and generate the XCode project using GYP:

    GYP_PATH/gyp --depth=. alchemy.gyp

# Dependencies

Below are instructions to manually build Alchemy's dependencies. Visit the Alchemy's download page for prebuilt dependencies.

`libcef`, `libcef_dll_wrapper`, and the Boost headers are required by Alchemy, while Google Test and the Boost `filesystem` and `system` libraries are only needed for Alchemy's tests and the File Finder example project, respectively.

## Chromium Embedded Framework

### libcef

A prebuilt binary of `libcef` is provided by Marshall A. Greenblatt, owner of the Chromium Embedded Framework project. You can [get it here](http://www.magpcss.net/cef_downloads/). The current version used by Alchemy is named `cef_binary_1.1364.1123_macosx.zip`, but newer versions should work.

Download and extract the archive to a temporary directory.

Navigate to the `libalchemy` directory within your workspace. Create a directory here named `cef` if one does not yet exist. This is where we will place all of the binaries and headers needed for the Chromium Embedded Framework.

From the `cef_binary_1.1364.1123_macosx` directory, copy the `include` directory into `libalchemy\cef\`. (So you should now have, `libalchemy\cef\include`.)

Create a new directory, `runtime`, in `libalchemy\cef`. Copy everything except `cefclient.exe` from `cef_binary_1.1364.1123_macosx\Release` into the `libalchemy\cef\runtime` directory.

Copy `libcef.lib` from `cef_binary_1.1364.1123_macosx\lib\Release` into `libalchemy\cef`.

### libcef\_dll\_wrapper

The last CEF dependency is `libcef_dll_wrapper.lib`. This must be built with the Xcode Command Line Tools compiler.

Navigate in your terminal to the `cef_binary_1.1364.1123_macosx` directory.

Type the command `xcodebuild cefclient.xcodeproj`.

TODO: MAC BUILD INSTRUCTIONS FOR DLL WRAPPER

## Google Test

If you wish to run or contribute to our unit test project, you will need to use Google Test. 

We have included pre-built binaries for Google Test that will work for any 32-bit machine. This can be found on the Alchemy [downloads page](<%= url('/downloads/') %>).

If you would like to build Google Test yourself, the Google Test source can be [downloaded here](https://code.google.com/p/googletest/downloads/list) from Google Code.

You can follow Google Test's [documentation](https://code.google.com/p/googletest/wiki/Documentation) to build and deploy Google Test on your own.

Copy both `libgtest.a` and `libgtest_main.a` from `gtest-1.6.0/xcodebuild/gtest/Release` to `libalchemy/gtest`.
You may need to create a directory in `libalchemy` named `gtest` if one does not yet exist.

Copy the `gtest` directory from `gtest-1.6.0` to `libalchemy/gtest`. (So you should now have `libalchemy/gtest/gtest`, containing Google Test's headers.)

Google Test is now configured for Alchemy.

## Boost

The Boost headers are required by Alchemy, however if you would like to run the File Finder example project, you will also need the Boost `system` and `filesystem` libraries.

Visit the [Boost Downloads page](http://www.boost.org/users/download/) and under `Current Release`, click the `Download` link. This will take you to SourceForge.

Download the Boost archive. [boost\_1\_53\_0.zip](http://sourceforge.net/projects/boost/files/boost/1.53.0/boost_1_53_0.zip/download) was used for Alchemy.

Extract Boost to a directory of your choice.

### Installing Boost Headers

***TODO:** Install ONLY Boost headers*

### Building Boost Libraries: filesystem and system

The File Finder example project requires two Boost libraries: filesystem and system. Here are instructions to build them.

Open command prompt and navigate to the `boost_1_53_0` folder.

Enter the command

    ./bootstrap.sh --prefix=/usr/local --with-libraries=filesystem,system

Once this completes execute the following command to build our libraries:

    ./b2 -a toolset=clang cxxflags="-std=c++11 -stdlib=libc++ -arch i386" linkflags="-stdlib=libc++ -arch i386" threading=multi link=static architecture=x86 install

Once the libraries are built, they will be placed into `/usr/local` and are ready to go.