---
title: GYP
show_toc: true
---

# GYP

GYP (which stands for **G**enerate **Y**our **P**roject) is a meta-build tool used in many Chromium related projects to build platform-specific C++ projects, similar to cmake or AutoConf.

On Mac it generates an XCode project, on Linux it generates Makefiles, and on Windows it generates a Visual Studio project.

# Requirements

GYP requires a Python 2.x runtime (NOT version 3). You can download it from the [Python downloads page](http://python.org/download/).

# Obtaining GYP

## Linux

On Ubuntu, and probably most other distributions, there is already a package for GYP available for your package manager. Try `sudo apt-get install gyp` or the equivalent.

If you can't find a package, continue reading.

## Our Barebones Package

For your convenience, we've put together a barebones package of GYP, containing only the essentials you need to use it.

You can download it [here](https://bitbucket.org/libalchemy/libalchemy/downloads/gyp.tar.gz), then unzip it to a directory of your choice.

## Via SVN

GYP is hosted through their [GoogleCode project](https://code.google.com/p/gyp/), and if you want the most up-to-date version of GYP available, you can checkout a copy of their source code:

    svn checkout http://gyp.googlecode.com/svn/trunk/ gyp

Note that this requires you to install [Subversion](http://subversion.tigris.org/).

# Using GYP

Firstly, keep note of where you put your GYP download, you'll need this path to run the script. I recommend putting it either in a higher level directory like a `projects/` folder or your home folder, or directly in the folder of the project you're working with. I'll refer to this path as `GYP_PATH`. If you installed via a package manager, or have otherwise installed it system-wide, you should just be able to use it directly without any path.

The most basic way to use GYP is with this command:

    GYP_PATH/gyp --depth=.

If you execute that from a directory containing a `*.gyp` file (We just refer to these as "GYP files"), GYP will generate your project. The `--depth=.` flag is a workaround for what appears to be quirk in GYP's design.

If you need to specify a specific GYP file to use, just tell it:

    GYP_PATH/gyp --depth=. filename.gyp

Finally, if you are using GYP to generate a project for Alchemy or one of its example projects on Windows, we need to force GYP to target the VS 2012 compiler:

    GYP_PATH/gyp --depth=. -G msvs_version=2012