---
title: File Finder Tutorial
show_toc: true
---

# Introduction

This tutorial is intended to provide a walk through to the Alchemy example project File Finder. This tutorial will assume that you have gone through the Getting Started page and your specific operating system's setup page. If you have not done so already it is recommended that you go through those before continuing any further with this tutorial.

File Finder is what we consider our moderate tutrial for using Alchemy, if you have not done so already it is recommended that you complete the Owl Facts tutorial prior to completing this tutorial.

You can fork or clone the File Finder project from our [BitBucket repository](http://bitbucket.org/libalchemy/filefinder).

# What is File Finder?

File Finder is the moderate tutorial for using Alchemy. It is a program that shows off the following features of Alchemy:

* The use of a C++ back end to drive an HTML/Javascript/CSS front end.
* The front end (or UI) calling functions defined in the back end.
* The front end displaying the results of the back end calls.
* The back end calling functions defined in the front end.
* The back end using data given from the front end.
* The use of threading in an Alchemy project.
* The use of third party libraries in both the front and back end in conjunction with Alchemy.

As for what exactly File Finder does, it is a basic directory parser. Given a directory from the front end, the back end will parse the directory using the Boost library and will give the front end all files that it finds within the directory.

# The Back End

To start we will take a look at the back end C++ code.

The only file for the back end is main.cpp. An abbreviated version is shown here:

~~~ cpp
#include "alchemy/alchemy.h"
#include <boost/filesystem.hpp>

#include <functional>
#include <iostream>

using namespace std;
using namespace std::placeholders;

struct file {
  string fullpath;
  string filepath;
  int size;
};

/* Globals */
unique_ptr<Alchemy::Application> app;
bool dirCancel = false;
vector<file> masterList;
string dir = "";
string oldDir = " ";
bool first = true;
mutex dirCancelMutex;

/* Methods */

//Called from front end. Sets directory and a directory cancel flag.
void updateDirectory(string directory) {
  ...
}

//Does the actual work of clearing master list, and checking if it is actually a directory.
void backUpdateDirectory(){
  ...
}

//Adds a file to master list and front end.
void addFile(file f) {
  ...
}

//Removes files from front end.
void emptyFrontEnd(){
  ...
}

//The file searching algorithm.
void fileSearch(){
  ...
}

int main(){
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(1100, 800));

  app = Alchemy::Application::Create(settings);

  auto index = app->Page("index.html");
  index->Bind("updateDirectory", updateDirectory);
  index->Bind("getExecutableFolder", Alchemy::GetExecutableFolder);
  dir = Alchemy::GetExecutableFolder();
  
  auto dirthread = thread([&](){
    while(!index->Ready()){
      this_thread::sleep_for(chrono::milliseconds(100));
    }
    fileSearch();
  });

  app->Show(index);
  dirthread.join();
  return 0;
}
~~~

Note that this is an extremely abbreviated version of main.cpp. Important methods will be further examined later in this tutorial, but this gives you a high level view of the program.

Now lets break this down.

The first thing to notice is what we are including.

~~~ cpp
#include "alchemy/alchemy.h"
#include <boost/filesystem.hpp>

#include <functional>
#include <iostream>

using namespace std;
using namespace std::placeholders;
~~~

First up is the Alchemy header. This allows us to use Alchemy.

Next is the Boost filesystem header. This allows us to use Boost's filesystem functionality.

The next two are functional and iostream, both which allow us to use some basic c++ capabilites.

Finally the by using the namespace std and std::placeholders we are saving ourselves some time by not having to prefix any standard c++ items.

Up next we have our functions that really drive File Finder.

~~~ cpp
struct file {
  string fullpath;
  string filepath;
  int size;
};

/* Globals */
unique_ptr<Alchemy::Application> app;
bool dirCancel = false;
vector<file> masterList;
string dir = "";
string oldDir = " ";
bool first = true;
mutex dirCancelMutex;

/* Methods */

//Called from front end. Sets directory and a directory cancel flag.
void updateDirectory(string directory) {
  ...
}

//Does the actual work of clearing master list, and checking if it is actually a directory.
void backUpdateDirectory(){
  ...
}

//Adds a file to master list and front end.
void addFile(file f) {
  ...
}

//Removes files from front end.
void emptyFrontEnd(){
  ...
}

//The file searching algorithm.
void fileSearch(){
  ...
}
~~~

Here we see a few things. First is a struct we have created called file. This struct holds some information about a file that we will need for the front and back end. This information is the full path of the file, the file path of the file, and the size of the file.

Next are a few global variables that File Finder will use. The app variable is our handle into the Alchemy [Application object](#). We also have a boolean value to trigger a directory change, a file vector to hold all the files found, a string to hold the directory, a string to hold the last god directory, a boolean to mark whether it is the first run of the directory crawl, and a mutext to protect our directory change boolean variable.

Now we will break down each function:

* updateDirectory - A method called from the front end that will take a string representing the new directory and will update the back end to match. This method does very little as explained in the next function.
* backUpdateDirectory - Does the actual updating of the directory. This is done to keep the front end from haning upon the call to updateDirectory.
* addFile - Adds a found file to the master list of files as well as the front end.
* emptyFrontEnd - Empties the front end of all files.
* fileSearch - the actual file searching algorithm. This will be done in a separate thread.

Now to take a closer look at a few of the more important functions in File Finder.

First up is updateDirectory:

~~~ cpp
//Called from front end. Sets directory and a directory cancel flag.
void updateDirectory(string directory) {
  if(!first){
    dirCancelMutex.lock();
    dirCancel = true;
    dirCancelMutex.unlock();
    oldDir = dir;
    dir = directory;
  } else {
    first = false;
  }
}
~~~

This is the only method File Finder that is called from the front end. It is a pretty basic method that takes a given directory, sets our flag to let the search method know to stop, and sets our new directory.

Next is backUpdateDirectory:

~~~ cpp
//Does the actual work of clearing master list, and checking if it is actually a directory.
void backUpdateDirectory(){
  boost::filesystem::path p(dir);
  if(boost::filesystem::is_directory(p)){
    emptyFrontEnd();
    masterList.clear();
    dirCancelMutex.lock();
    dirCancel = false;
    dirCancelMutex.unlock();
  } else {
    dir = oldDir;
    emptyFrontEnd();
    masterList.clear();
    app->Page("index.html")->Call("showError", "Not a directory, displaying last known good directory.");
  }
}
~~~

Once the main searching thread has been notified of a change in directory, this method is called. This method takes care of actually emptying the master list of files and resets the directory cancel flag so that the search can start again. This method is also interesting because it is the first time that the back end actually calls a front end function.

In the case of a directory error we need to notify the front end that an error has occured. To do this we use the app Application objct we created, grab the page that we would like to call a function from, and use the Call function to call the front end method showError, and pass it any arguments that it may have. In this case an error message to display.

Next is addFile:

~~~ cpp
//Adds a file to master list and front end.
void addFile(file f) {
  masterList.push_back(f);
  app->Page("index.html")->Call("addFile", f.fullpath, f.size);
}
~~~

Here we have an extremely simple function. Whenever the back end search thread finds a file it will call addFile. This method, given the file, will add that file to the master file list, and then add that file to the front end.

This is another example of calling a function on the front end. Again we use the app Application object, grab the page we would like to call the function from, and the use the Call function to call the front end method addFile. We then pass it any arguments it may need. In this case the fullpath of the file and the file size.

We will now take a quick look at the fileSearch method:

~~~ cpp
//The file searching algorithm.
void fileSearch(){
  while (true) {
    stack<boost::filesystem::path> stack;
    boost::filesystem::path p(dir);
    stack.push(p);
    while (stack.size() > 0) {
      boost::filesystem::path p = stack.top();
      stack.pop();
      boost::filesystem::directory_iterator end_itr;
      for(boost::filesystem::directory_iterator itr(p); itr != end_itr; ++itr) {
        if (boost::filesystem::is_directory(itr->path())) {
          stack.push(itr->path());
        } else {
            try{
              file f;
              f.size = boost::filesystem::file_size(itr->path());
              f.filepath = boost::filesystem::basename(itr->path()) + boost::filesystem::extension(itr->path());
              f.fullpath = itr->path().string();
              addFile(f);
            } catch (const boost::filesystem::filesystem_error& ex){}
        }
      }
    }
    while(true){
      dirCancelMutex.lock();
      if(dirCancel != false){
        dirCancelMutex.unlock();
        backUpdateDirectory();
        break;
      }
      dirCancelMutex.unlock();
    }
  }
}
~~~

This method never directly calls a front end function but is important to note that we are using the Boost filesystem header to handle the searching of the directories.

This was chosed because Boost is a very mature C++ library and it takes care of handling all of the cross platform directory quirks for us. As you can see Alchemy will work with most third party libraries without any problems.

These functions are the real backbone of File Finder. If you would like to look at any of the other functions in more detail please check out the source code.

Finally we come to the main method of File Finder.

~~~ cpp
int main(){
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(1100, 800));

  app = Alchemy::Application::Create(settings);

  auto index = app->Page("index.html");
  index->Bind("updateDirectory", updateDirectory);
  index->Bind("getExecutableFolder", Alchemy::GetExecutableFolder);
  dir = Alchemy::GetExecutableFolder();
  
  auto dirthread = thread([&](){
    while(!index->Ready()){
      this_thread::sleep_for(chrono::milliseconds(100));
    }
    fileSearch();
  });

  app->Show(index);
  dirthread.join();
  return 0;
}
~~~

The first thing we do here is create an Alchemy [Settings object](#). These are the settings that we will use to create a window. With this object we set a few options such as the directory that Alchemy will look for your UI files, and what size we would like the window to be created at.

Next we actually create the window using the Application create method and we pass in the settings object we just created and modified.

The next few lines of code:

~~~ cpp
auto index = app->Page("index.html");
index->Bind("updateDirectory", updateDirectory);
index->Bind("getExecutableFolder", Alchemy::GetExecutableFolder);
dir = Alchemy::GetExecutableFolder();
~~~

are setting up the pages avalable in File Finder as well as binding our back end functions that the front end will need.

Using the auto type to determine what kind of variable we are creating, we can set the pages by making a call to the Application objects Page method. This is essentially binding the given page to our Alchemy Application object. With our newly created variables we can now directly reference the pages instead of going through the Application object (which you could do if you wanted to).

The next two lines of code are using our reference to the index page to bind the back end function updateDirectory and the Alchemy function getExecutableFolder to the front end.

Finally we set the back ends starting directory to the executable folder of FileFinder using Alchemy's built in getExecutableFolder function.

Up next we spawn a thread to start doing the directory search.

~~~ cpp
auto dirthread = thread([&](){
  while(!index->Ready()){
    this_thread::sleep_for(chrono::milliseconds(100));
  }
  fileSearch();
});
~~~

This thread will wait for the index page to become ready and the it will start the file search.

Finally we call the Show method of our Application object passing in the page we would like to be initiall shown.

~~~ cpp
app->Show(index);
dirthread.join();
~~~

Finally once the application is closed the join the file searching thread to cleanly close the program.

And that wraps up the back end. It is a fairly simple back end and integrating Alchemy into it comes down to only a few lines of code as shown in the main method, and within the methods calling out to the front end.

# The Front End

It is important to note that this tutorial will only cover the JavaScript of the front end. If you would like to take a look at the HTML or CSS please check out the source files.

Here is an abbreviated version of the JavaScript file for File Finder. It uses [KnockoutJS](http://knockoutjs.com) for binding all the file data to HTML documents and taking care of events and propagating changes.

~~~ js
function front_to_back(name) {
  if (!window.app) window.app = {};
  if (!window.app[name]) window.app[name] = function () {
    console.log('call back-end',name,arguments);
  };
}
function back_to_front(name, f) {
  if (!window.app) window.app = {};
  if (!window.app[name]) window.app[name] = f.bind(ffVM);
}

FileFinderViewModel.prototype = {
  openFile: function(result) {
    this.selectedResult(result);
    $('#fileview').hide().fadeIn();
  },

  showError: function(message) {
    TINY.box.show({
      html: '<strong class="text-error">'+message+'</strong>',
      close: false,
      boxid: 'errormessage'
    });
  },
};

front_to_back('updateDirectory');
front_to_back('getExecutableFolder');
back_to_front('addFile', ffVM.addResult);
back_to_front('showError', ffVM.showError);

$(function(){
  ko.applyBindings(ffVM);
  $('._fancy input').fancyInput();
  ffVM.directory.subscribe(app.updateDirectory);
  var dir = app.getExecutableFolder();
  ffVM.directory(dir);
  fancyInput.fillText(dir, document.getElementById('folder-input'));
});
~~~

The functions front_to_back, and back_to_front are shortcut functions that will help us when binding functions. These are not necessary for back to front calling with Alchemy but they work well with the view model architecture of File Finder.

When the back end calls a function on the front end (as shown in the back end section of this tutorial) Alchemy will call whatever function specified.

The only other interesting parts of this front end are the calls from the front end to the back end.

This occurs with the calls app.getExecutableFolder and app.updateDirectory. In both of these cases a simple call to the function using the app object allows us to cal any functions that the back end has bound.

# Wrap Up

Now that we have gone through the File Finder example here is a review of some of the things this tutorial has gone through.

* How to create and show a window using Alchemy.
* How to bind a back end function to the front end.
* How to call the bound function from the front end.
* How to call a front end function from the back end.
* Use of third part libraries in conjunction with Alchemy.
