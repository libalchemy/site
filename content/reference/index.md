---
title: Reference
---
# Reference

This section serves as the official user manual to Alchemy, and includes detailed descriptions of each of the main classes in Alchemy, how to build Alchemy, how to build projects with Alchemy, and more.