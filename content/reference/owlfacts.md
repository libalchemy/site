---
title: Owl Facts Tutorial
show_toc: true
---

# Introduction

This tutorial is intended to provide a walk through to the Alchemy example project Owl Facts. This tutorial will assume that you have gone through the Getting Started page and your specific operating system's setup page. If you have not done so already it is recommended that you go through those before continuing any further with this tutorial.

You can fork or clone the Owl Facts project from our [BitBucket repository](http://bitbucket.org/libalchemy/owlfacts).

# What is Owl Facts?

Owl Facts is the beginner tutorial for using Alchemy. It is a very simple program that shows off the following features of Alchemy:

* The use of a C++ back end to drive an HTML/Javascript/CSS front end.
* The front end (or UI) calling functions defined in the back end.
* The front end displaying the results of the back end calls.

As for what exactly Owl Facts does, it contains a two pages home.html which will display a user name defined in the back end, and facts.html which will display a ramdom fact about owls which is generated from the back end.

# The Back End

To start we will take a look at the back end C++ code.

The only file for the back end is main.cpp. An abbreviated version is shown here:

~~~ cpp
#include <iostream>
#include <string>

#include "alchemy/alchemy.h"

using namespace std;

unique_ptr<Alchemy::Application> app;
vector<string> owlFacts;
int currentFact = 0;

string getUserName() {
  return "DenverCoder9";
}

string getRandomOwlFact() {
  currentFact %= owlFacts.size();
  return owlFacts[currentFact++];
}

void addFacts() {
  //Builds the list of owl facts.
}

int main() {
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(800, 600));
  app = Alchemy::Application::Create(settings);

  addFacts();

  auto home  = app->Page("home.html");
  auto facts = app->Page("facts.html");
  auto index = app->Page("index.html");

  home->Bind("getUserName", getUserName);
  facts->Bind("getRandomOwlFact", getRandomOwlFact);
  
  app->Show(index);

  return 0;
}
~~~

Now lets break this down.

The first thing to notice is what headers we are including.

~~~ cpp
#include <iostream>
#include <string>

#include "alchemy/alchemy.h"
#include "alchemy/js/js.h"

using namespace std;
~~~

The first two headers included, iostream and string, are basic c++ headers that give us access to strings and iostream capabilities.

Up next are the Alchemy headers alchemy.h and js.h. These will allow us to actually use Alchemy.

Finally the by using the namespace std we are saving ourselves some time by not having to prefix any standard c++ items.

Up next we have our the functions that really drive Owl Facts.

~~~ cpp
unique_ptr<Alchemy::Application> app;
vector<string> owlFacts;
int currentFact = 0;

string getUserName() {
  return "DenverCoder9";
}

string getRandomOwlFact() {
  currentFact %= owlFacts.size();
  return owlFacts[currentFact++];
}

void addFacts() {
  //Builds the list of owl facts.
}
~~~

Here we see a few things. First are a few global variables that Owl Facts will use. The app variable is our handle into the Alchemy [Application object](#). We also have a string vector to hold our owl facts, and a integer currentFact to keep track of what fact we are currently viewing.

Now we will break down each function:

* getUserName - Returns a string user name that is currently set as "DenverCoder9".
* getRandomOwlFact - Returns a string owl fact that is randomly chosen from our list of owl facts.
* addFacts - Adds a few facts to the owl facts list so that we can view them later. Note that the actual adding of the facts was stripped out for purpose of not cluttering up this tutorial and can be found in the actual source code.

These functions are the real backbone of Owl Facts. It is the methods getUserName and getRandomOwlFact that will ultimately be called from the front end.

Finally we come the the main method of Owl Facts.

~~~ cpp
int main() {
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(800, 600));
  app = Alchemy::Application::Create(settings);

  addFacts();

  auto home  = app->Page("home.html");
  auto facts = app->Page("facts.html");
  auto index = app->Page("index.html");

  home->Bind("getUserName", getUserName);
  facts->Bind("getRandomOwlFact", getRandomOwlFact);
  
  app->Show(index);

  return 0;
}
~~~

The first thing we do here is create an Alchemy [Settings object](#). These are the settings that we will use to create a window. With this object we set a few options such as the directory that Alchemy will look for your UI files, and what size we would like the window to be created at.

Next we actually create the window using the Application create method and we pass in the settings object we just created and modified.

Up next is a call to the addFacts method to populate our list of owl facts.

The next three lines of code:

~~~ cpp
auto home  = app->Page("home.html");
auto facts = app->Page("facts.html");
auto index = app->Page("index.html");
~~~

are setting up all of the pages that will be available in Owl Facts.

Using the auto type to determine what kind of variable we are creating, we can set the pages by making a call to the Application objects Page method. This is essentially binding the given page to our Alchemy Application object. With our newly created variables we can now directly reference the pages instead of going through the Application object (which you could do if you wanted to).

The next two lines of code are perhaps the most interesting of the whole project.

~~~ cpp
home->Bind("getUserName", getUserName);
facts->Bind("getRandomOwlFact", getRandomOwlFact);
~~~

Here, using our references to the pages, we are actually binding the back end functions getUserName and getRandomOwlFact to the front end.

We pass in the name of the function to be bound to the front end, and the function pointer to be invoked.

Finally we call the Show method of our Application object passing n the page we would like to be initially shown.

And that wraps up the back end. It is a fairly simple back end and integrating Alchemy into it comes down to only a few lines of code as shown in the main method.

# The Front End

It is important to note that all of the javascript shown in this example is contained within script tags of the HTML. Becasue this is a simple example this made for a few less source files, bigger applications would most likely have their javascript contained in a seperate file.

Here are the front end files in all their glory:

index.html

~~~ html
<html>
  <head>
    <title>Demo App</title>
    <link rel="stylesheet" type="text/css" href="main.css">
  </head>
  <body>
    <div class="menu">
      <a href="home.html" target="content">Home</a>, 
      <a href="facts.html" target="content">Facts</a>
    </div>
    <iframe name="content" src="home.html"></iframe>
  </body>
 </html>
~~~

home.html

~~~ html
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="page.css">
    <script type="text/javascript">
      window.onload = function() {
        document.getElementById('user').innerHTML = app.getUserName();
      };
    </script>
  </head>
  <body>
    Welcome, <span id="user" style="font-weight: bold;">User Placeholder</span>, to Owl Facts!
    <br>
    <br>
    Here you will find my favorite facts about owls.
    <br>
    <span style="padding-left: 150px;">- Dr. Alfax Ph.D</span>
  </body>
</html>
~~~

facts.html

~~~ html
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="page.css">
    <script type="text/javascript">
      window.onload = getRandomOwlFact;

      function getRandomOwlFact() {
        document.getElementById('fact').innerHTML = app.getRandomOwlFact();
      }
    </script>
  </head>
  <body>
    <button onclick="getRandomOwlFact();">New Fact!</button>
    <br>
    <br>
    <div id="fact">Fact Placeholder</div>
  </body>
</html>
~~~

The CSS files will not be shown in this tutorial as their sole purpose is to make the pages look pretty. If you would like to take a look at those check them out in the source code.

To start off we will take a quick look at the index.html file.

There is not much to say about this page as its only purpose is to dispaly the home.html and facts.html pages. If you do not understand what is happening with this page check out some HTML tutorials before proceeding as you will most likely not understand the other pages as well.

In the home.html file we begin to see where Alchemy comes into play.

While most of the file is basic HTML there is a portion that deserves a little bit of explanantion:

~~~ html
  <script type="text/javascript">
    window.onload = function() {
      document.getElementById('user').innerHTML = app.getUserName();
    };
  </script>
</head>
<body>
  Welcome, <span id="user" style="font-weight: bold;">User Placeholder</span>, to Owl Facts!
~~~

The first thing to look at is the span with id "user". This is eventually where we would like to display the user name as returned from our back end function getUserName.

The most important part however is the script tag. Within this tag we see a function that will be called upon the window loading.  In this function we grab the element "user" and by using the app, Alchemy application object, we can call the method getUserName on the back end. This method call will return our user name from the back end and place it within the "user" span. It is as simple as that!

Finally we will take a look at the facts.html page.

As with home.html most of the file is basic HTML with the exception of a few important bits.

~~~ html
  <script type="text/javascript">
    window.onload = getRandomOwlFact;

    function getRandomOwlFact() {
      document.getElementById('fact').innerHTML = app.getRandomOwlFact();
    }
  </script>
</head>
<body>
  <button onclick="getRandomOwlFact();">New Fact!</button>
  <br>
  <br>
  <div id="fact">Fact Placeholder</div>
~~~

The first thing to look at is the div with id "fact". This is eventually where we would like to display our randomly given owl fact from the back end funtion getRandomOwlFact.

The most important part however is the script tag. Within this tag we see a function that will be called upon the window loading, as well as any time the get next fact button is clicked.  In this function we grab the element "fact" and by using the app, Alchemy application object, we can call the method getRandomOwlFact on the back end. This method call will return our random owl fact from the back end and place it within the "fact" div. Every time the page is reloaded, or the get next fact button is pressed a new fact will be given to the front end from the back end.

# Wrap Up

Now that we have gone through the Owl Facts example here is a review of some of the things this tutorial has gone through.

* How to create and show a window using Alchemy.
* How to bind a back end function to the front end.
* How to call the bound function from the front end.

To see how to bind and call a function that resides on the front end, and to see a more complex example please see the File Finder tutorial.