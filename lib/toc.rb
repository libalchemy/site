require 'nokogiri'

def toc
  content = @rep.content[:last]
  doc = Nokogiri::HTML(content)
  h = '<ul class="nav-toc">'
  h << '<li class="sect">Table of Contents</li>'

  last_was_h1 = false
  last_was_h2 = false
  doc.xpath("//*[name()='h1' or name()='h2']").each do |header|
    is_h1 = header.node_name == 'h1'
    is_h2 = header.node_name == 'h2'

    if last_was_h1 && is_h2
      h << '<ul>'
    end

    if last_was_h2 && is_h1
      h << '</li></ul></li>'
    end

    if (last_was_h2 && is_h2) || (last_was_h1 && is_h1)
      h << '</li>'
    end

    h << '<li><a href="#' << (header['id']||'')
    h << '">' << header.text << '</a>'


    last_was_h1 = is_h1
    last_was_h2 = is_h2
  end

  if last_was_h2
    h << '</li></ul>'
  end
  h << '</li></ul>'
  h
end