def page(ident, title, *subs)
  Page.new(@item.identifier == ident, @items[ident], title, subs).tap{|p| subs.each{|s| s.parent = p}}
end

def url(ident)
  @items[ident].reps[0].path
end

class Page
  attr_reader :title, :item, :subpages
  attr_accessor :parent

  def initialize(is_active, item, title, subs)
    @title = title
    @item = item
    @subpages = subs
    @is_active = is_active
  end

  def tap
    yield self
    self
  end

  def is_active?
    @is_active || subpages.any?(&:is_active?)
  end

  def to_s
    h = '<li'
    h << ' class="active"' if is_active?
    h << '><a href="' << item.rep_named(:default).path << '">'
    h << title << '</a>'
    if subpages.length > 0 && is_active?
      h << '<ul>'
      subpages.each{|s| h << "\n" << s.to_s}
      h << '</ul>'
    end
    h << '</li>'
  end
end