# Compilation Instructions

1. Install Ruby 1.9 or higher
2. Install the Rubygems `nanoc`, `kramdown`, `mime-types`, `nokogiri`, and `adsf`:

        gem install nanoc kramdown mime-types nokogiri adsf

3. Run `nanoc compile` to compile
4. (Optional) Run `nanoc view` to serve the site on localhost:3000

# Compilation Rules

* Any files in the `content` directory get processed as website content
* Any `*.md` files will be parsed as an ERB document, then a Kramdown document, before being saved as a `.html` file
* Any `*.less` file will be parsed as a LESS-CSS document and then saved as a `.css` file
* By adding the `show_toc: true` property to a pages meta-data, the document will be scanned for first and second level headers, and a table of contents will be displayed. This code is located in `lib/toc.rb`
* The navigation sidebar is generated from the code in `layouts/navigation.rb`, using code in `lib/nav.rb`
